<div class="view view-unswer view-id-unswer view-display-id-page_1 view-dom-id-1">
  <div class="views-row views-row-1 views-row-odd views-row-first">
     <div class="views-field-nothing">
        <span class="field-content">
           <div class="qestions-ask-top">
              <div class="qestions-ask-date"><?php echo format_date($node->created, 'custom', 'd F Y'); ?></div>
              <div class="qestions-ask-user"><?php echo $node->field_nameask[0]['value']; ?></div>
           </div>
           <div class="qestions-ask-qestions">
              <?php echo $node->field_ask[0]['value']; ?>
           </div>
           <div class="qestions-ask-bottom">
              <div class="qestions-ask-answer-user-top">
                 <div class="qestions-ask-answer-user"><?php echo $node->field_namemanager[0]['value']; ?></div>
                 <div class="qestions-ask-answer-user-arrow"></div>
              </div>
              <div class="qestions-ask-answer">
                 <div class="qestions-ask-answer-inner">
                  <?php echo $node->field_unswer[0]['value']; ?>
                 </div>
              </div>
           </div>
        </span>
     </div>
  </div>
</div>