<?php include('header.php'); ?>        
  <div class="inner-blocks">
    <div class="wrapper-inner">
      
      <div class="inner-left">  
	<div class="catalogue-blocks">
	  <div class="catalogue-block first">
	      <?php
		$tree = taxonomy_get_tree(7,0,-1);
		foreach($tree as $key => $term) {				
		  if(!$key) {
		    $link = '<div><a href="/bytovye">Смотреть все товары раздела</a></div>';
		    echo '<div><a href="/bytovye">Бытовые</a></div>';
		    echo '<img src="/sites/all/themes/osushitel/img/catalogue-block-1.png">';
		  } else {
		    echo '<div>'.l($term->name, taxonomy_term_path($term)).'</div>';
		  }
		}
		echo $link;
	      ?>
	  </div>
	  <div class="catalogue-block bg">
	      <?php
		$tree = taxonomy_get_tree(9,0,-1);
		foreach($tree as $key => $term) {				
		  if(!$key) {
		    $link = '<div><a href="/dlya-basseinov">Смотреть все товары раздела</a></div>';
		    echo '<div><a href="/dlya-basseinov">Для бассейнов</a></div>';
		    echo '<img src="/sites/all/themes/osushitel/img/catalogue-block-2.png">';
		  } else {
		    echo '<div>'.l($term->name, taxonomy_term_path($term)).'</div>';
		  }
		}
		echo $link;
	      ?>
	  </div>
	  <div class="catalogue-block">
	      <?php
		$tree = taxonomy_get_tree(11,0,-1);
		foreach($tree as $key => $term) {		
		  echo '<div>'.l($term->name, taxonomy_term_path($term)).'</div>';
		  if(!$key) {
		    $link = '<div>'.l('Смотреть все товары раздела', taxonomy_term_path($term)).'</div>';
		    echo '<img src="/sites/all/themes/osushitel/img/catalogue-block-3.png">';
		  }
		}
		echo $link;
	      ?>
	  </div>	
	</div>
	<div class="catalogue-brands">
	  <ul>
	    <?php
	      $tree = taxonomy_get_tree(10,0,-1);
	      $num = 1;
	      foreach($tree as $key => $term) {
		if($key){
		  $fields = term_fields_get_fields_values($term);
		  if($num==3) $class = 'no-margin'; else $class = '';
		  $image = '';
		  if(isset($fields['brand_logo_fid']) && $fields['brand_logo_fid']) {
		    $fid = $fields['brand_logo_fid'];
		    $logo = field_file_load($fid);
		    $image = theme('imagecache','brand_logo',$logo['filepath']);
		  }
		  echo '<li class="'.$class.'">'.l($image.$term->name, taxonomy_term_path($term), array('html' => TRUE)).'</li>';		  
		  $num++;
		  if($num==4) $num = 1;
		}
	      }
	    ?>
	  <ul>
	</div>	
      </div>      
      <div class="inner-right">
	<div class="block-right">
	  <div class="block-title">Акционные товары</div>
	  <?php echo views_embed_view('product_frontpage','default');?>
	</div>
      </div>
      
      <?php /*
      <?php print $filter; ?>
      <div class="product">
	  <?php BuildCatalog(); ?>
      </div>
      */?>
    </div>
  </div>
<?php include('footer.php'); ?>