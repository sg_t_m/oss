<?php include('header.php'); ?>  
  <div class="inner-blocks margin-inner">
    <div class="wrapper-inner">
      <div class="node-tabs">
	 <ul>
	   <li><a href="/article">Блог</a></li>
	   <li><a href="/news" class="active">Новости</a></li>
	   <li><a href="/portfolio">Реализованные проекты</a></li>
	 </ul>
	 <div class="created"><?php echo format_date($node->created, $type = 'custom', 'd F Y'); ?></div>
      </div>      
      <?php echo views_embed_view('lists','default', 'all', 'news_block');?>
    </div>
  </div>
<?php include('footer.php'); ?>
