  <div class="footer">
    <div class="wrapper-inner">
      <div class="copyright">
	©<?php echo date('Y'); ?> osushiteli.ua<br>Все права защищены.
	<div class="footer-phone-number"><span>(044)</span> 361-55-65</div>
	<a href="https://plus.google.com/101375933440996954472" rel="publisher" target="_blank"><img src="/sites/all/themes/osushitel/img/ico-google.png" width="24" height="24"></a>
	<a href="http://www.youtube.com/channel/UCSdPX3NL0bUlhWXwlM9jMOg" target="_blank"><img src="/sites/all/themes/osushitel/img/ico-youtube.png" width="24" height="24"></a>
      </div>
      <div class="footer-about">
	<h3>О компании</h3>
	<div class="footer-about-body">
	    <?php print $footerabout; ?>
	</div>
      </div>
      <div class="footer-catalog">
	<h3>Каталог</h3>
	<ul class="footer-catalog-nav">
	<?php
	  $tree = taxonomy_get_tree(9,0,-1);
	  foreach($tree as $key => $term) echo '<li>'.l($term->name,'taxonomy/term/'.$term->tid).'</li>';
	?>
	</ul>
      </div>
      <div class="footer-info-menu">
	<h3>Информация</h3>
	<?php print theme('links', menu_navigation_links('secondary-links'),array('ul' => 'nav')); ?>
      </div>
    </div>
  </div>
</div>

   
<style>
</style>
  <?php echo $styles ?>
 <?php echo $scripts ?>
  <?php print $closure; ?>
  <script type="text/javascript">
  (function (d, w, c) {
  (w[c] = w[c] || []).push(function() {
  try {
  w.yaCounter19209811 = new Ya.Metrika({id:19209811,
  webvisor:true,
  clickmap:true,
  trackLinks:true,
  accurateTrackBounce:true});
  } catch(e) { }
  });  
  var n = d.getElementsByTagName("script")[0],
  s = d.createElement("script"),
  f = function () { n.parentNode.insertBefore(s, n); };
  s.type = "text/javascript";
  s.async = true;
  //s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
  s.src ="/sites/default/files/e_js/watch.js";
  if (w.opera == "[object Opera]") {
  d.addEventListener("DOMContentLoaded", f, false);
  } else { f(); }
  })(document, window, "yandex_metrika_callbacks");
  </script>
  <noscript><div><img src="//mc.yandex.ru/watch/19209811" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->
   <div style="display: none;">
    <div id="cartcontent" class="checkout-cart-block">
      <h2 class="cart-title">Ваша корзина</h2>
      <div class="cart-data">
	<div class="cart-data-products-list"></div>
	<?php
	  module_load_include('inc', 'uc_cart', 'uc_cart_checkout_pane');
	  module_load_include('inc', 'uc_cart', 'uc_cart.ca');
	  module_load_include('inc', 'uc_cart', 'uc_cart.pages');
	  //$items = uc_cart_get_contents();
	  //if(count($items)):
	    echo '<div class="cart-wrapper-checkout">';
	      echo '<div class="cart-wrapper-checkout-title">Пожалуйста, заполните поля для доставки оплаты товара</div>';
	      echo uc_cart_checkout();
	    echo '</div>';
	  //endif;
	?>
      </div>
    </div>
  </div>
  <div style="display: none;">
    <div id="callbackcontent" class="callback-wrapper">
      <?php print $callback; ?>
      <?php echo drupal_get_form ('virdini_current_application_form') ?>
    </div>
  </div>
 
  <!-- Код тега ремаркетинга Google -->
<!--------------------------------------------------
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 861660289;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/861660289/?guid=ON&amp;script=0"/>
</div>
</noscript>
   
</body>
</html>