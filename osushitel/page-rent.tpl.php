<?php include('header.php'); ?>
  <div class="inner-blocks margin-inner">
    <div class="wrapper-inner">
      <div class="global-region">      	
	<div class="page-rent-inner">    
	  <div class="rent-before">
	    <?php print $rent_top; ?>
	  </div>
	  <?php print $content; ?>
	  <br/>
	  <br/>
	  <div class="rent-after">
	    <?php print $rent_bottom; ?>	   
	  </div>
	</div>
      </div>
    </div>
  </div>
<?php include('footer.php'); ?>