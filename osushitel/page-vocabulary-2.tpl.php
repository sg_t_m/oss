<?php include('header.php'); ?>  
  <div class="inner-blocks margin-inner">
    <div class="wrapper-inner">
      <div class="global-region-left">
	  <ul class="left-menu">
	  <?php
	    function category_term_depth($tid) {
	      $parent = db_result(db_query("SELECT parent FROM {term_hierarchy} WHERE tid=%d", $tid));
	      if($parent == 0) {
		return 1;
	      }else  {
		return 1+category_term_depth($parent);
	      }
	    }    
	    $tree = taxonomy_get_tree(13,0,-1);
	    foreach($tree as $key => $term) {		
	      echo '<li class="level-'.category_term_depth($term->tid).'">'.l($term->name, taxonomy_term_path($term)).'</li>';
	    }      
	  ?>
	  </ul>
	</div>
	<div class="global-region-right">
	  <?php echo views_embed_view('lists','default', arg(2));?>
	</div>
    </div>
  </div>
<?php include('footer.php'); ?>
