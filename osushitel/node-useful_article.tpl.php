<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
  <div class="global-region-left">
    <ul class="left-menu">
    <?php 
      /*$tree = taxonomy_get_tree(13,0,-1);
      foreach($tree as $key => $term) {		
        echo '<li class="level-'.node_term_depth_article($term->tid).'">'.l($term->name, taxonomy_term_path($term)).'</li>';
      }*/
      $query = "SELECT * FROM {node} WHERE status = 1 AND type IN ('useful_article') ORDER BY created DESC";
      $query = db_query($query);
      while ($inode = db_fetch_object($query)) {
        echo '<li class="level-3">'.l($inode->title, drupal_get_path_alias('node/'.$inode->nid)).'</li>';
      }      
    ?>
    </ul>
  </div>
  <div class="global-region-right">
    <div class="node-tabs">
      <ul>
        <li><a href="/article" class="active">Блог</a></li>
        <li><a href="/news">Новости</a></li>
        <li><a href="/portfolio">Реализованные проекты</a></li>
      </ul>
      <div class="created"><?php echo format_date($node->created, $type = 'custom', 'd F Y'); ?></div>
    </div>
    <div class="node-content">
      <?php /*if(isset($node->field_news_image[0]['filepath'])): ?>
        <img src="/<?php echo $image_path = imagecache_create_path('product_list',$node->field_news_image[0]['filepath']); ?>" alt="<?php print $node->title; ?>" title="<?php print $node->title; ?>" class="node-image">
      <?php endif;*/ ?>
      <div class="node-header">
        <h1 class="node-title"><?php print $title; ?></h1>
        <div class="social">
          <?php echo theme_social_share('googleplus', $node);
          echo theme_social_share('facebook', $node);
          ?></div>
        <div class="node-icons">          
          <div class="node-icon node-icon-user">Осушители</div>
          <div class="node-icon node-icon-views"><?php echo calculator_nodeview_count($node->nid); ?></div>
        </div>  
      </div>
      <div class="node-text">
        <?php echo CleanContent($content, $node->nid); ?>    
      </div>
    </div>
    <div class="ask-news-blocks">
      <div class="news-articles-block">
        <ul class="tabs">
          <li class="active"><a href="#">Новости</a></li>
          <li><a href="#">Статьи</a></li>
        </ul>
        <ul class="tabs-content">
          <li class="active">
            <a href="/news" class="tabs-link">Смотреть все новости</a>
            <?php echo views_embed_view('news_block','default');?>
          </li>
          <li><?php echo views_embed_view('article_block','default');?></li>
        </ul>
      </div>
    </div>    
  </div>
</div>