<?php include('header.php'); ?>
        <div id="left-column">
	    <?php if($user->uid) { ?>
	    <div class="admin-menu">
		<div class="title-block">admin menu</div>
		<?php print theme('links', menu_navigation_links('navigation')); ?>
	    </div>
	    <?php } ?>
            <div class="filter">
                <div class="title-block">каталог осушителей</div>
                <div class="product-filter">
                    <div class="item">
                        <div class="section first">
                            <div class="text">Для бассейнов</div>
                        </div>
                        <ul>
                            <?php
			    $tree = taxonomy_get_tree(1,0,-1,1);
			    foreach($tree as $key => $term){
			    echo '<li>'.l($term->name,'taxonomy/term/'.$term->tid).'</li>';
			    }
			    ?>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="section">
                            <div class="text">Промышленные</div>
                        </div>
                        <ul>
                            <?php
			    $tree = taxonomy_get_tree(2,0,-1,1);
			    foreach($tree as $key => $term){
			    echo '<li>'.l($term->name,'taxonomy/term/'.$term->tid).'</li>';
			    }
			    ?>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="section">
                            <div class="text">Все производители</div>
                        </div>
                        <ul>
                           <?php
			    $tree = taxonomy_get_tree(3,0,-1,1);
			    foreach($tree as $key => $term){
			    echo '<li>'.l($term->name,'taxonomy/term/'.$term->tid).'</li>';
			    }
			    ?>
                        </ul>
                    </div>  
                </div>
            </div>
            <div class="ban">
                <div class="inner">
                    <?php print $firstbanner; ?>
                </div>
            </div>
                <div class="poll-block">
                <div class="title-block">опрос</div>
		<?php print $poll; ?>
            </div>
        </div>
	<div id="content">

	    <?php print $breadcrumds; ?>
		<h1><?php print $title; ?></h1>
	    
		<?php
			    module_load_include('module', 'ubercart', 'uc_cart');
			    echo drupal_get_form('uc_cart_view_form');
			    ?>

	</div>
        <?php include('footer.php'); ?>