<?php if(!$teaser): ?>
  <div class="node-product">
    <h1 class="product-title"><?php print $title; ?></h1>
    <div class="node-product-left">
      <div class="img-box">
	<a class="colorbox-product" href="/<?php echo $node->field_image_cache[0]['filepath']; ?>">
	  <span class="zoom"></span>
	  <img src="/<?php echo $image_path = imagecache_create_path('product_full',$node->field_image_cache[0]['filepath']); ?>" alt="<?php print $title; ?>" title="<?php print $title; ?>" rel="group" class="imagecache imagecache-product">
	</a>
      </div>
      <?php if(isset($node->field_image_cache[1]['filepath'])): ?>
	<?php foreach($node->field_image_cache as $key=>$imagesmall): ?>
	  <?php //if($key): ?>
	    <div class="img-box-small">
	      <a class="colorbox-product" href="/<?php echo $imagesmall['filepath']; ?>">
		<img src="/<?php echo $image_path = imagecache_create_path('product_full',$imagesmall['filepath']); ?>" alt="<?php print $title; ?>" title="<?php print $title; ?>" rel="group" class="imagecache imagecache-product">
	      </a>
	    </div>	  
	  <?php //endif; ?>
	<?php endforeach; ?>
	<div class="clearfix"></div>
      <?php endif; ?>
      <?php  if($node->field_video[0]['view']): ?>
	<?php foreach ($node->field_video as $item) { ?>
	  <div class="video-box">
	    <a class="youtube colorbox-load" href="http://www.youtube.com/embed/<?php print $item['value'] ?>?width=724&height=424&iframe=true&autoplay=1&rel=0&wmode=transparent" title="<?php print $node->field_video_name[0]['view']; ?><?php echo htmlspecialchars('<a href="/'.drupal_get_path_alias('node/'.$node->nid).'" class="colorbox-button">Посмотреть осушитель</a>', ENT_QUOTES, "UTF-8"); ?>">
		<span class="video-title"><?php echo $node->field_video_name[0]['view']; ?></span>
		<span class="video-screenshot">
		    <img class="group1 youtubeplay" src="http://img.youtube.com/vi/<?php print $item['value'] ?>/0.jpg" alt="<?php echo $node->field_video_name[0]['view']; ?>"/><span></span>
		</span>
	    </a>
	  </div>
	<?php } ?>
      <?php endif; ?>
      <div class="product-links">
	<ul>
	  <?php if($node->field_services_installation[0]['view']): ?>
	     <li><a href="/ustanovka-montazh" target="_blank" class="services-install"><img src="/sites/all/themes/osushitel/img/ico-product-install.png">Потребуются услуги монтажа</a></li>
	  <?php endif; ?>
	  <?php if($node->field_design_services[0]['view']): ?>
	    <li><a href="/montazh-i-proektirovanie" target="_blank" class="design-services"><img src="/sites/all/themes/osushitel/img/ico-product-project.png">Потребуются услуги проектирования</a></li>
	  <?php endif; ?>
	  <?php if($node->field_instruction[0]['view']): ?>
	    <li><a href="<?php echo $node->field_instruction[0]['view']; ?>" target="_blank" class="instr"><img src="/sites/all/themes/osushitel/img/ico-product-pdf.png">Скачать инструкцию</a></li>
	  <?php endif; ?>
	  <?php if($node->field_suitable[0]['view']): ?>
	    <li><a href="/calculator?performance=<?php echo strip_tags($node->field_performance[0]['view']); ?>&name=<?php echo urlencode(htmlentities(strip_tags($node->title),1)); ?>" class="dehumidifier"><img src="/sites/all/themes/osushitel/img/ico-product-calc.png">Проверить - подходит-ли осушитель</a></li>
	  <?php endif; ?>
	  <li><a href="/dostavka-i-oplata" target="_blank" class="delivery-time"><img src="/sites/all/themes/osushitel/img/ico-product-delivery.png">Срок поставки<span><?php echo $node->field_delivery[0]['view']; ?></span></a></li>
	</ul>
      </div>
      <div class="ask-form">
	<div class="form-title">Задать вопрос или оставить отзыв</div>
	<?php echo drupal_get_form ('virdini_ask_application_form', $node) ?>
      </div>
    </div>  
    <div class="node-product-right">
      <div class="node-product-right-left">
	<div class="node-product-price">				
	  <div class="node-product-price-uah">
	    <?php
	      $context = array(
		'revision' => 'themed',
		'type' => 'product',
		'class' => array('product'),
	      );
	      $options = array('label' => FALSE);
	      $context['class'][1] = 'sell';
	      $context['field'] = 'sell_price';
	      echo uc_price($node->sell_price, $context, $options);
	    ?>
	  </div>
	  <div class="node-product-price-usd">
	    <?php
	      if($node->field_currency[0]['value']!='USD'){
		$price = calculator_exchanges_money($node->sell_price, $node->field_currency[0]['value'], 'USD');
	      } else {
		$price = $node->sell_price;
	      }
	      $price = number_format($price, 0, '.', ' ');
	      echo '$'.$price;
	    ?>
	  </div>
	  <div class="node-product-price-eur">
	    <?php
	      if($node->field_currency[0]['value']!='EUR'){
		$price = calculator_exchanges_money($node->sell_price, $node->field_currency[0]['value'], 'EUR');
	      } else {
		$price = $node->sell_price;
	      }
	      $price = number_format($price, 0, '.', ' ');
	      echo '€'.$price;
	    ?>
	  </div>	  	  
	</div>	
	<div class="node-product-buy">
	  <?php echo $node->content['add_to_cart']['#value']; ?>
	  <a href="#" class="phone-call">Купить по телефону</a>
	  <a href="#" class="ask-call">Задать вопрос</a>
	</div>
	<?php if(isset($node->field_action_date[0]['value'])): ?>
	  <?php if($node->field_action_date[0]['value']>time()): ?>
	  <?php $days = round(($node->field_action_date[0]['value']-time())/86400); ?>
	  <div class="node-product-days">
	    Осталось
	    <div><?php echo $days; ?></div>
	    <?php echo product_plural($days, array('ден','дня','дней')); ?>
	  </div>
	  <?php endif; ?>
	<?php endif; ?>
	<div class="node-product-parameters">
	  <h1>Технические характеристики</h1>
	  <ul>
	    <?php  if($node->field_apartments_up[0]['view']): ?>
	    <li>
		<b>Для квартир площадью до</b><span><b><?php echo $node->field_apartments_up[0]['view']; ?></b></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_area[0]['view']): ?>
	    <li>
		<b>Для бассейнов с площадью до</b><span><b><?php echo $node->field_area[0]['view']; ?></b></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_rooms[0]['view']): ?>
	    <li>
		<b>Для помещений объемом до</b><span><b><?php echo $node->field_rooms[0]['view']; ?></b></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_performance[0]['view']): ?>
	    <li>
		Производительность при 30°/80% (л/сут)<span><?php echo $node->field_performance[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_humidity[0]['view']): ?>
	    <li>
		Рабочий диапазон, влажность(% RH)<span><?php echo $node->field_humidity[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_range[0]['view']): ?>
	    <li>
		Рабочий диапазон, температура (°C)<span><?php echo $node->field_range[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_air_flow[0]['view']): ?>
	    <li>
		Расход воздуха (м3/ч)<span><?php echo $node->field_air_flow[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_pressure[0]['view']): ?>
	    <li>
		Макс. внешнее статическое давление (Па)<span><?php echo $node->field_pressure[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_fresh_air[0]['view']): ?>
	    <li>
		Приток свежего воздуха (м3/ч)<span><?php echo $node->field_fresh_air[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_exit_pressure[0]['view']): ?>
	    <li>
		Давление на выходе (Па)<span><?php echo $node->field_exit_pressure[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>                                
	    <?php  if($node->field_coolant[0]['view']): ?>
	    <li>
		Хладагент<span><?php echo $node->field_coolant[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_protection[0]['view']): ?>
	    <li>
		Класс защиты<span><?php echo $node->field_protection[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_power[0]['view']): ?>
	    <li>
		Параметры электропитания (В/Гц)<span><?php echo $node->field_power[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_consumption_current[0]['view']): ?>
	    <li>
		Макс. потребляемый ток (А)<span><?php echo $node->field_consumption_current[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_power_consumption[0]['view']): ?>
	    <li>
		Макс. потребляемая мощность (кВт)<span><?php echo $node->field_power_consumption[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_refrigerant[0]['view']): ?>
	    <li>
		Количество хладагента (кг)<span><?php echo $node->field_refrigerant[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_noise_level[0]['view']): ?>
	    <li>
		Уровень шума (3м) – (дБ(А))<span><?php echo $node->field_noise_level[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_weight[0]['view']): ?>
	    <li>
		Вес (кг)<span><?php echo $node->field_weight[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_height_product[0]['view']): ?>
	    <li>
		Высота (мм)<span><?php echo $node->field_height_product[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_width_product[0]['view']): ?>
	    <li>
		Ширина (мм)<span><?php echo $node->field_width_product[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_depth_product[0]['view']): ?>
	    <li>
		Глубина (мм)<span><?php echo $node->field_depth_product[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	  </ul>    
	  <?php  if($node->field_heat_pump[0]['view'] || $node->field_water_heater[0]['view'] || $node->field_only[0]['view'] ): ?>
	    <h2>Нагрев воздуха:</h2>
	  <?php endif; ?>
	  <ul class="first-block">
	    <?php  if($node->field_heat_pump[0]['view']): ?>
	    <li>
		Через тепловой насос (режим B) (кВт)<span><?php echo $node->field_heat_pump[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_water_heater[0]['view']): ?>
	    <li>
		Через LPHW (опция) – водяной нагреватель при 80°C (кВт)<span><?php echo $node->field_water_heater[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_only[0]['view']): ?>
	    <li>
		Всего (режим В + LPHW) (кВт)<span><?php echo $node->field_only[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	  </ul>
	  <?php  if($node->field_pump[0]['view'] || $node->field_water[0]['view'] || $node->field_all[0]['view'] ): ?>
	    <h2>Нагрев воды:</h2>
	  <?php endif; ?>
	  <ul>
	    <?php  if($node->field_pump[0]['view']): ?>
	    <li >
		Через тепловой насос (Режим А) (кВт)<span><?php echo $node->field_pump[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_water[0]['view']): ?>
	    <li>
		Через LPHW (опция) – водяной нагреватель при 80°C (кВт)<span><?php echo $node->field_water[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_all[0]['view']): ?>
	    <li>
		Всего (режим А + LPHW) (кВт)<span><?php echo $node->field_all[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_water_pool[0]['view']): ?>
	    <li>
		Расход воды бассейна (л/мин)<span><?php echo $node->field_water_pool[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_pressur[0]['view']): ?>
	    <li>
		Макс. рабочее давление (бар)<span><?php echo $node->field_pressur[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_pressure_drop[0]['view']): ?>
	    <li>
		Падение давления при номинальном расходе (бар)<span><?php echo $node->field_pressure_drop[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	  </ul>
	  <?php  if($node->field_amper[0]['view'] || $node->field_watt[0]['view'] ): ?>
	    <h2>Электрические данные для параметров 1*230/50:</h2>
	  <?php endif; ?>
	  <ul>
	    <?php  if($node->field_amper[0]['view']): ?>
	    <li>
		Макс. потребляемый ток (А) <span><?php echo $node->field_amper[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_watt[0]['view']): ?>
	    <li>
		Макс. потребляемая мощность (кВт)<span><?php echo $node->field_watt[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	  </ul>
	  <?php  if($node->field_mamper[0]['view'] || $node->field_kwatt[0]['view'] ): ?>
	    <h2>Электрические данные для параметров 3*400/50:</h2>
	  <?php endif; ?>
	  <ul>
	    <?php  if($node->field_mamper[0]['view']): ?>
	    <li>
		Макс. потребляемый ток (А)<span><?php echo $node->field_mamper[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	    <?php  if($node->field_kwatt[0]['view']): ?>
	    <li>
		Макс. потребляемая мощность (кВт)<span><?php echo $node->field_kwatt[0]['view']; ?></span>
	    </li>
	    <?php endif; ?>
	  </ul>
	</div>
        <?php  if($node->field_features[0]['view']): ?>
	  <div class="node-product-block">	
	    <div class="h2">Конструктивные и функциональные особенности</div>                            
	    <?php echo $node->field_features[0]['view']; ?>
	  </div>
	<?php endif; ?>
    <?php  if($node->field_action_new[0]['view']): ?>
    <style>
    #node-product-action{ border: solid 2px #699ec1;
    padding: 20px;
    width:90%;
    position: relative;}
   
    #node-product-action-l {
    display: block;
    width: 39px;
    height: 38px;
    position: absolute;
    top: -20px;
    left: -20px;
    background-image: url(/sites/all/themes/osushitel/img/rm.png);
}
    </style>
	  <div class="node-product-block" id="node-product-action" style="">	
	  <div id="node-product-action-l"> </div> 
      <div class="h2">Опачки, подарочек!</div>                              
	    <?php echo $node->field_action_new[0]['view']; ?>
	  </div>
	<?php endif; ?>
    
	<?php  if($node->content['body']['#value']): ?>
	  <div class="node-product-block">	
	    <div class="h2">Описание</div>                            
	    <?php print $node->content['body']['#value']; ?>
	  </div>
	<?php endif; ?>
      </div>
      <div class="node-product-right-right">
	<?php if(isset($node->field_credit[0]['value'])): ?>
	  <?php if($node->field_credit[0]['value']==1): ?>
	  <div class="node-product-credit">
	    <div class="node-product-credit-title">Или <a href="#">оплатить частями</a>:</div>
	    <div class="node-product-credit-line">
	      <div class="node-product-credit-line-left">
		<span>3</span> платежа
	      </div>
	      <div class="node-product-credit-line-right">
		по <?php
		$context = array(
		  'revision' => 'themed',
		  'type' => 'product',
		  'class' => array('product'),
		);
		$options = array('label' => FALSE);
		$context['class'][1] = 'sell';
		$context['field'] = 'sell_price';
		echo uc_price($node->sell_price/3, $context, $options);
	      ?>
	      </div>
	    </div>
	    <div class="node-product-credit-line">
	      <div class="node-product-credit-line-left">
		<span>6</span> платежей
	      </div>
	      <div class="node-product-credit-line-right">
		по <?php
		$context = array(
		  'revision' => 'themed',
		  'type' => 'product',
		  'class' => array('product'),
		);
		$options = array('label' => FALSE);
		$context['class'][1] = 'sell';
		$context['field'] = 'sell_price';
		echo uc_price($node->sell_price/6, $context, $options);
	      ?>
	      </div>
	    </div>
	    <div class="node-product-credit-line">
	      <div class="node-product-credit-line-left">
		<span>12</span> платежей
	      </div>
	      <div class="node-product-credit-line-right">
		по <?php
		$context = array(
		  'revision' => 'themed',
		  'type' => 'product',
		  'class' => array('product'),
		);
		$options = array('label' => FALSE);
		$context['class'][1] = 'sell';
		$context['field'] = 'sell_price';
		echo uc_price($node->sell_price/12, $context, $options);
	      ?>
	      </div>
	    </div>	  
	    <a href="#">Выбрать рассрочку или кредит</a>
	  </div>
	  <?php endif; ?>
	<?php endif; ?>
	
	<div class="node-product-delivery">
	  <h2><img src="/sites/all/themes/osushitel/img/ico-delivery.png">Доставка</h2>
	  <div class="open-text">
	    <?php echo str_replace('kiev.ua','ua',$node->field_delivery_text[0]['view']); ?>
	  </div>
	</div>
	<div class="node-product-varianty">
	  <h2><img src="/sites/all/themes/osushitel/img/ico-varianty.png">Гарантия</h2>
	  <div class="open-text">
	    <?php echo str_replace('kiev.ua','ua',$node->field_warranty[0]['view']); ?>
	  </div>								
	</div>
	
	<div class="node-product-country">
	  <div class="h2">Особенности модели</div>
	  <ul>
	    <?php if($node->field_manufacturer_country[0]['view']): ?>
	      <li>
		<img src="/sites/all/themes/osushitel/img/ico-country.png">Изготовлено:<span><?php echo $node->field_manufacturer_country[0]['view']; ?></span>
	      </li>
	    <?php endif; ?>
	    <?php if($node->field_country[0]['view']): ?>
	      <li>
		<img src="/sites/all/themes/osushitel/img/ico-manufacturer.png">Сборка:<span><?php echo $node->field_country[0]['view']; ?></span>
	      </li>
	    <?php endif; ?>
	  </ul>
	</div>

	<?php  if($node->field_application[0]['view']): ?>
	  <div class="node-product-block">	
	    <div class="h2">Применение</div>                          
	    <?php echo $node->field_application[0]['view']; ?>
	  </div>
	<?php endif; ?>
	
      </div>
    </div>
  </div>
<?php endif; ?>
			

 